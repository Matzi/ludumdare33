﻿using UnityEngine;
using System.Collections;

public class Music : MonoBehaviour {
    private static Music instance = null;

    void Awake()
    {
        if (instance != null && instance != this) {
            Destroy(this.gameObject);
            return;
        } else {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }
}
