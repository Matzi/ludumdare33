﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour
{

    public Map UsedMap;

    protected int CurrentLevel;

    void BackToMain()
    {
        Application.LoadLevel("MainMenu");
    }

    void LevelSelector()
    {
        Application.LoadLevel("LevelSelector");
    }

    void Credits()
    {
        Application.LoadLevel("Credits");
    }

    void HowToPlay()
    {
        Application.LoadLevel("HowToPlay");
    }

    void Quit()
    {
        Application.Quit();
    }

    void Retry()
    {
        Application.LoadLevel("GameScene");
    }

    void NextLevel()
    {
        ++CurrentLevel;
        PlayerPrefs.SetInt("Current Level", CurrentLevel);
        Application.LoadLevel("GameScene");
    }

    void NewGame() { PlayerPrefs.SetInt("Current Level", 0); Application.LoadLevel("Tutorial"); }
    void GoLevel1() { PlayerPrefs.SetInt("Current Level", 0); Application.LoadLevel("GameScene"); }
    void Level2() { PlayerPrefs.SetInt("Current Level", 1); Application.LoadLevel("GameScene"); }
    void Level3() { PlayerPrefs.SetInt("Current Level", 2); Application.LoadLevel("GameScene"); }
    void Level4() { PlayerPrefs.SetInt("Current Level", 3); Application.LoadLevel("GameScene"); }
    void Level5() { PlayerPrefs.SetInt("Current Level", 4); Application.LoadLevel("GameScene"); }
    void Level6() { PlayerPrefs.SetInt("Current Level", 5); Application.LoadLevel("GameScene"); }
    void Level7() { PlayerPrefs.SetInt("Current Level", 6); Application.LoadLevel("GameScene"); }
    void Level8() { PlayerPrefs.SetInt("Current Level", 7); Application.LoadLevel("GameScene"); }
    void Level9() { PlayerPrefs.SetInt("Current Level", 8); Application.LoadLevel("GameScene"); }
    void Level10() { PlayerPrefs.SetInt("Current Level", 9); Application.LoadLevel("GameScene"); }
    void Level11() { PlayerPrefs.SetInt("Current Level", 10); Application.LoadLevel("GameScene"); }
    void Level12() { PlayerPrefs.SetInt("Current Level", 11); Application.LoadLevel("GameScene"); }

    void Start()
    {
        CurrentLevel = PlayerPrefs.GetInt("Current Level", 0);
        if (UsedMap != null)
        {
            if (CurrentLevel < 12)
            {
                Map map = Instantiate(UsedMap).GetComponent<Map>();

                map.NumWarrior = NumWarrior[CurrentLevel];
                map.NumRogue = NumRogue[CurrentLevel];
                map.NumWizard = NumWizard[CurrentLevel];
                map.NumCleric = NumCleric[CurrentLevel];
                map.NumBossWarrior = NumBossWarrior[CurrentLevel];
                map.NumBossRogue = NumBossRogue[CurrentLevel];
                map.NumBossWizard = NumBossWizard[CurrentLevel];
                map.NumBossCleric = NumBossCleric[CurrentLevel];
                map.DimX = DimX[CurrentLevel];
                map.DimY = DimY[CurrentLevel];
                map.WallRatio = WallRatio[CurrentLevel];
                map.ConnectionRatio = ConnectionRatio[CurrentLevel];
                map.BlockedRatio = BlockedRatio[CurrentLevel];
            }
            else
            {
                Application.LoadLevel("Victory");
            }
        }
    }

    private int[] NumWarrior ={ 2, 1, 2, 4, 5, 5, 3, 4, 2, 3, 4, 4 };
    private int[] NumRogue =  { 0, 3, 0, 0, 3, 2, 3, 3, 2, 4, 3, 4 };
    private int[] NumWizard = { 0, 0, 2, 0, 1, 2, 2, 2, 2, 2, 2, 3 };
    private int[] NumCleric = { 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 3, 3 };
    private int[] NumBossWarrior ={ 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0 };
    private int[] NumBossRogue =  { 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0 };
    private int[] NumBossWizard = { 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1 };
    private int[] NumBossCleric = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1 };
    private int[] DimX = { 04, 06, 06, 06, 08, 08, 10, 10, 10, 12, 12, 12 };
    private int[] DimY = { 06, 08, 10, 10, 10, 12, 12, 14, 16, 16, 18, 20 };
    private float[] WallRatio = { 0.9f, 0.95f, 0.9f, 0.99f, 0.9f, 0.7f, 0.85f, 0.9f, 0.95f, 0.9f, 0.95f, 0.9f };
    private float[] ConnectionRatio = { 0.6f, 0.7f, 0.8f, 0.6f, 0.7f, 0.8f, 0.6f, 0.7f, 0.8f, 0.8f, 0.8f, 0.8f };
    private float[] BlockedRatio = { 0.1f, 0.1f, 0.15f, 0.1f, 0.1f, 0.15f, 0.1f, 0.1f, 0.15f, 0.1f, 0.1f, 0.15f };
}
