﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ButtonScript : MonoBehaviour {
    public int Level = 0;

	// Use this for initialization
	void Start () {
        int UnlockedLevel = PlayerPrefs.GetInt("Unlocked Level", 0);
        if (Level > UnlockedLevel)
        {
            GetComponent<Button>().interactable = false;
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
