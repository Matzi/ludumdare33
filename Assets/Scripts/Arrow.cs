﻿using UnityEngine;
using System.Collections;
using System;

public class Arrow : MonoBehaviour
{
    public bool CanTurn = false;
    public float MaximumLifetime = 10.0f;

    protected float LifeTime;
    protected float Damage;
    protected GameObject Player;

    // Use this for initialization
    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
        LifeTime = MaximumLifetime;
    }

    // Update is called once per frame
    void Update()
    {
        if (Player != null && !Player.GetComponent<Player>().IsDead() && CanTurn)
        {
            Vector3 targetDirection = (Player.transform.position - transform.position);
            targetDirection.y = 0;

            Rigidbody rigidbody = GetComponent<Rigidbody>();
            rigidbody.MoveRotation(Quaternion.LookRotation(targetDirection));
            float mag = rigidbody.velocity.magnitude;
            rigidbody.velocity = transform.forward * mag;
        }
    }

    void FixedUpdate()
    {
        LifeTime -= Time.fixedDeltaTime;
        if (LifeTime < 0.0f)
        {
            SelfDestruct();
        }
    }

    void SelfDestruct()
    {
        Destroy(gameObject, 3);
        Destroy(gameObject.GetComponent<Rigidbody>());
        Destroy(gameObject.GetComponent<Collider>());
        Destroy(gameObject.GetComponent<Light>());
        gameObject.GetComponent<ParticleSystem>().Stop();
        Destroy(this);
    }

    void OnCollisionEnter(Collision col)
    {
        GameObject obj = col.collider.gameObject;
        Creature creature = obj.GetComponent<Creature>();

        if (obj.CompareTag("Trap"))
        {
            return;
        }

        if (creature != null)
        {
            creature.Damage(Damage);
        }
        SelfDestruct();
    }

    public void SetDamage(float damage)
    {
        Damage = damage;
    }
}
