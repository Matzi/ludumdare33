﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Opponent : Creature
{
    protected float ReturnTimer = 4.0f;

    protected Vector3 TargetPosition;
    protected GameObject Player;
    protected List<Vector3> Path = new List<Vector3>();
    protected TextMesh HP;
    protected float AvoidanceTimer;
    protected bool ShouldRun = false;
    protected float WanderTimer = 0;
    protected Guide Team;

    // Use this for initialization
    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
        TargetPosition = transform.position;
        HP = GetComponentInChildren<TextMesh>();

        //Path = Map.GetComponent<Map>().GetPathTo(transform.position, Player.transform.position, false);
        Init();
        Stop();
    }

    protected override void LastUpdate()
    {
        if (HP != null)
        {
            HP.text = "" + ActualHealth;
        }

        Team.Dead++;
    }

    protected override void FixedStep()
    {
        if (HP != null)
        {
            HP.text = "" + ActualHealth;
        }

        if (!IsAlive)
        {
            return;
        }

        AvoidanceTimer -= Time.fixedDeltaTime;
        WanderTimer -= Time.fixedDeltaTime;

        if (transform.position.y < 0.0f)
        {
            Vector3 pos = transform.position;
            pos.y = 0.1f;
            transform.position = pos;
        }

        if (CanMove())
        {
            Vector3 targetDirection = (TargetPosition - transform.position);
            targetDirection.y = 0;

            if (targetDirection.sqrMagnitude > 0.1f)
            {
                WanderTimer = ReturnTimer;
                Move(ShouldRun);
                Vector3 origin = transform.position;
                float moveSpeed = Speed * Time.fixedDeltaTime;

                if (targetDirection.sqrMagnitude > moveSpeed * moveSpeed)
                {
                    targetDirection.Normalize();
                    GetComponent<CharacterController>().Move(targetDirection * (ShouldRun ? 1.0f : 0.45f) * moveSpeed - Vector3.up);
                }
                else
                {
                    GetComponent<CharacterController>().Move(targetDirection);
                }

                float dist = (origin - transform.position).magnitude;

                if (dist < moveSpeed * 0.2f && AvoidanceTimer <= 0.0f)
                {
                    TargetPosition = transform.position + transform.right * Random.Range(-4.0f, 4.0f);
                    AvoidanceTimer = 0.4f;
                    Path.Clear();
                }

                LookDirection = targetDirection;
            }
            else
            {
                Stop();
                if (Path.Count > 0)
                {
                    for (int l = 0; l < Path.Count - 1; ++l)
                    {
                        Vector3 dir = Path[l] - transform.position;
                        dir.y = 0.0f;
                        float distance = dir.magnitude;
                        dir.Normalize();
                        Ray ray = new Ray(transform.position + 2 * Vector3.up, dir);
                        RaycastHit hitInfo;

                        if (!Physics.SphereCast(ray, 1.5f, out hitInfo, distance, 1))
                        {
                            Path.RemoveRange(l + 1, Path.Count - l - 1);
                            break;
                        }
                    }

                    TargetPosition = Path[Path.Count - 1];
                    Path.RemoveAt(Path.Count - 1);
                }
            }
        }

        Vector3 direction;
        Creature creature = Player.GetComponent<Creature>();
        ShouldRun = false;

        if (CanMove() && (DisablesTrap || TreasureDetectionRadius > 1) && AvoidanceTimer <= 0.0f)
        {
            GameObject[] traps = GameObject.FindGameObjectsWithTag("Trap");
            foreach (GameObject obj in traps)
            {
                Trap trap = obj.GetComponent<Trap>();
                if (trap != null && (trap.transform.position - transform.position).sqrMagnitude < TreasureDetectionRadius * TreasureDetectionRadius)
                {
                    Vector3 dir;
                    if (InDirectLineOfSight(obj, out dir, TreasureDetectionRadius) && (DisablesTrap || !trap.IsTrap))
                    {
                        TargetPosition = trap.transform.position;
                    }
                }
            }
        }

        if (creature != null && !creature.IsDead() && CanSee(creature, out direction) && AvoidanceTimer <= 0.0f)
        {
            ShouldRun = true;
            TurnInvisible();
            LookDirection = direction;
            if (CanAttack() && !Attack(creature))
            {
                TargetPosition = creature.transform.position;
                Path.Clear();
            }
            else
            {
                TargetPosition = transform.position;
            }
        }

        if (!Moving && WanderTimer < 0.0f && (Team.transform.position - transform.position).sqrMagnitude > 9.0f && Map.GetComponent<Map>() != null)
        {
            Path = Map.GetComponent<Map>().GetPathTo(transform.position, Team.transform.position, false);
        }
    }

    public void SetGuid(Guide guide)
    {
        Team = guide;
    }
}
