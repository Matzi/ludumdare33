﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Assets.Scripts;
using System.Collections.Generic;
using System;

using Random = UnityEngine.Random;

public class Map : MonoBehaviour
{
    public GameObject[] Floors;
    public GameObject[] Walls;
    public GameObject[] Corners;
    public GameObject[] OpponentType;
    public GameObject[] BossType;
    public GameObject Cover;
    public GameObject PlayerType;
    public GameObject Entrance;
    public GameObject Exit;
    public int DimX;
    public int DimY;
    public float BlockedRatio;
    public float WallRatio;
    public float ConnectionRatio = 1.0f;
    public Guide Team;
    public int NumWarrior = 3;
    public int NumRogue = 3;
    public int NumWizard = 2;
    public int NumCleric = 1;
    public int NumBossWarrior = 0;
    public int NumBossRogue = 0;
    public int NumBossWizard = 0;
    public int NumBossCleric = 0;
    public float GuideSpeed = 0.8f;

    private float SizeConstant = 3.0f;
    private Tile[][] Tiles;
    private int[] OffsetX = new int[8] { 0, 1, 1, 1, 0, -1, -1, -1 };
    private int[] OffsetY = new int[8] { -1, -1, 0, 1, 1, 1, 0, -1 };
    private float[] Rotation = new float[8] { 0, 0, -90, 0, 180, 0, -270, 0 };
    private int SizeX;
    private int SizeY;
    private int GenTiles = 0;
    private GameObject Victory;
    private GameObject Failure;

    bool Generate(int x, int y)
    {
        if (x < 1 || x > SizeX - 2 || y < 1 || y > SizeY - 2 || !Tiles[x][y].Solid || Tiles[x][y].Blocked)
        {
            return false;
        }

        GenTiles++;

        Tiles[x][y].Solid = false;
        Tiles[x + 1][y].Solid = false;
        Tiles[x][y + 1].Solid = false;
        Tiles[x + 1][y + 1].Solid = false;

        List<int> directions = new List<int>(new int[] { 0, 2, 4, 6 });

        int n = directions.Count;
        while (n > 1)
        {
            n--;
            int k = Random.Range(0, n + 1);
            int value = directions[k];
            directions[k] = directions[n];
            directions[n] = value;
        }

        for (int i = 0; i < 4; ++i)
        {
            int newDir = directions[i];

            if (Generate(x + 3 * OffsetX[newDir], y + 3 * OffsetY[newDir]))
            {
                Tiles[x + OffsetX[newDir]][y + OffsetY[newDir]].Solid = false;
                Tiles[x + 1 + OffsetX[newDir]][y + OffsetY[newDir]].Solid = false;
                Tiles[x + OffsetX[newDir]][y + 1 + OffsetY[newDir]].Solid = false;
                Tiles[x + 1 + OffsetX[newDir]][y + 1 + OffsetY[newDir]].Solid = false;
            }
        }

        return true;
    }

    void AddConnections()
    {
        int x = Random.Range(2, DimX - 2) * 3;
        int y = Random.Range(2, DimY - 2) * 3;
        int i = Random.Range(0, 3) * 2;

        if (Tiles[x + OffsetX[i]][y + OffsetY[i]].Solid &&
            Tiles[x + OffsetX[i] + 1][y + OffsetY[i]].Solid == Tiles[x + OffsetX[i] - 1][y + OffsetY[i]].Solid &&
            Tiles[x + OffsetX[i]][y + OffsetY[i] + 1].Solid == Tiles[x + OffsetX[i]][y + OffsetY[i] - 1].Solid &&
            Tiles[x + OffsetX[i] + 2][y + OffsetY[i]].Solid == Tiles[x + OffsetX[i] - 2][y + OffsetY[i]].Solid &&
            Tiles[x + OffsetX[i]][y + OffsetY[i] + 2].Solid == Tiles[x + OffsetX[i]][y + OffsetY[i] - 2].Solid &&
            Tiles[x + OffsetX[i]][y + OffsetY[i] + 1].Solid != Tiles[x + OffsetX[i] + 1][y + OffsetY[i]].Solid)
        {
            EatWall(x + OffsetX[i], y + OffsetY[i], 2);
        }
    }

    int GetNumberOfFloorTilesAround(int x, int y)
    {
        int num = 0;
        for (int i = 0; i < 8; i += 2)
        {
            if ((x + OffsetX[i]) >= 0 && (x + OffsetX[i]) < SizeX &&
                (y + OffsetY[i]) >= 0 && (y + OffsetY[i]) < SizeY &&
                !Tiles[x + OffsetX[i]][y + OffsetY[i]].Solid)
            {
                ++num;
            }
        }

        return num;
    }

    void EatWall(int x, int y, int limit = 9999)
    {
        Tiles[x][y].Solid = false;
        --limit;

        if (limit <= 0)
        {
            return;
        }

        for (int i = 0; i < 8; i += 2)
        {
            if ((x + OffsetX[i]) >= 1 && (x + OffsetX[i]) < SizeX - 1 &&
                (y + OffsetY[i]) >= 1 && (y + OffsetY[i]) < SizeY - 1 &&
                Tiles[x + OffsetX[i]][y + OffsetY[i]].Solid &&
                GetNumberOfFloorTilesAround(x + OffsetX[i], y + OffsetY[i]) >= 3)
            {
                EatWall(x + OffsetX[i], y + OffsetY[i], limit);
            }
        }
    }

    void GenerateRooms()
    {
        for (int x = 1; x < DimX; ++x)
        {
            for (int y = 1; y < DimY; ++y)
            {
                if (GetNumberOfFloorTilesAround(x * 3, y * 3) >= 3 && Random.Range(0.0f, 1.0f) > WallRatio)
                {
                    EatWall(x * 3, y * 3);
                }
            }
        }
    }

    void GenerateWalls()
    {
        for (int x = 1; x < SizeX - 1; ++x)
        {
            for (int y = 1; y < SizeY - 1; ++y)
            {
                if (!Tiles[x][y].Solid)
                {
                    int floorType = Math.Max(0, Random.Range(-12, Floors.Length));
                    Tiles[x][y].Floor = (GameObject)Instantiate(Floors[floorType], new Vector3(x * SizeConstant, 0f, y * SizeConstant), Quaternion.AngleAxis(Random.Range(0, 4) * 90, Vector3.up));
                    for (int i = 0; i < 8; i++)
                    {
                        if (Tiles[x + OffsetX[i]][y + OffsetY[i]].Solid)
                        {
                            if (i % 2 == 0)
                            {
                                int type = Math.Max(0, Random.Range(-8, Walls.Length));
                                Instantiate(Walls[type], new Vector3(x * SizeConstant, 0f, y * SizeConstant), Quaternion.AngleAxis(0, Vector3.left) * Quaternion.AngleAxis(Rotation[i], Vector3.up));
                            }
                            else
                            {
                                int left = (i + 1) % 8;
                                int right = (i + 7) % 8;
                                if (Tiles[x + OffsetX[left]][y + OffsetY[left]].Solid == Tiles[x + OffsetX[right]][y + OffsetY[right]].Solid)
                                {
                                    Instantiate(Corners[0], new Vector3((x + (OffsetX[i] * 0.5f)) * SizeConstant, 0f, (y + (OffsetY[i] * 0.5f)) * SizeConstant), Quaternion.identity);
                                }
                            }
                        }
                    }
                }
                else
                {
                    Tiles[x][y].Floor = (GameObject)Instantiate(Cover, new Vector3(x * SizeConstant, 0f, y * SizeConstant), Quaternion.AngleAxis(0, Vector3.up));
                }
            }
        }
    }

    void ClearZone(int x, int y, int w, int h)
    {
        for (int i = x; i < x + w; ++i)
        {
            for (int j = y; j < y + h; ++j)
            {
                Tiles[i][j].Solid = false;
            }
        }
    }

    // Use this for initialization
    void Start()
    {
        Victory = GameObject.Find("Victory");
        Victory.SetActive(false);
        Failure = GameObject.Find("Failure");
        Failure.SetActive(false);

        SizeX = DimX * 3 + 1;
        SizeY = DimY * 3 + 1;

        int StartX = (DimX / 2) * 3 + 1;
        int StartY = (DimY / 2) * 3 + 1;
        bool valid = false;

        Vector3 teamOffset = new Vector3(5 * SizeConstant, 0.0f, 5 * SizeConstant);
        Vector3 exitOffset = new Vector3((SizeX - 4) * SizeConstant, 0.0f, (SizeY - 4) * SizeConstant);

        List<Vector3> path;

        do
        {
            GenTiles = 0;
            Tiles = new Tile[SizeX][];

            for (int x = 0; x < SizeX; ++x)
            {
                Tiles[x] = new Tile[SizeY];
                for (int y = 0; y < SizeY; ++y)
                {
                    Tiles[x][y] = new Tile();
                    Tiles[x][y].X = x;
                    Tiles[x][y].Y = y;

                    if (Random.Range(0.0f, 1.0f) < BlockedRatio)
                    {
                        Tiles[x][y].Blocked = true;
                    }
                }
            }

            Tiles[StartX][StartY].Blocked = false;
            Generate(StartX, StartY);

            ClearZone(1, 1, 7, 7);
            ClearZone(SizeX - 9, SizeY - 9, 7, 7);

            path = GetPathTo(teamOffset, exitOffset, false);
            valid = path.Count > 4;
        }
        while (GenTiles < DimX * DimY / 4 || !valid);

        for (int i = 0; i < DimX * DimY * ConnectionRatio; ++i)
        {
            AddConnections();
        }

        GenerateRooms();

        GenerateWalls();

        Spawn(SizeX - 6, SizeY - 6, PlayerType);

        Vector3 offset = new Vector3(1 * SizeConstant, 0.0f, 3 * SizeConstant);
        Instantiate(Entrance, offset, Quaternion.identity);
    
        Instantiate(Exit, exitOffset, Quaternion.identity);

        Team = ((GameObject)Instantiate(Team.gameObject, teamOffset, Quaternion.identity)).GetComponent<Guide>();
        Team.SetPath(path);
        Team.GetComponent<Guide>().Speed = GuideSpeed;

        int[] opponentCount = new int[] { NumWarrior, NumRogue, NumWizard, NumCleric };
        int[] bossCount = new int[] { NumBossWarrior, NumBossRogue, NumBossWizard, NumBossCleric };

        for (int i = 0; i < opponentCount.Length && i < bossCount.Length && i < OpponentType.Length; ++i)
        {
            int counter = 0;
            while (counter < opponentCount[i])
            {
                int x = Random.Range(2, 5);
                int y = Random.Range(2, 5);

                if (!Tiles[x][y].Solid)
                {
                    Team.AddMember(Spawn(x, y, OpponentType[i]));
                    ++counter;
                }
            }

            counter = 0;
            while (counter < bossCount[i])
            {
                int x = Random.Range(2, 5);
                int y = Random.Range(2, 5);

                if (!Tiles[x][y].Solid)
                {
                    Team.AddMember(Spawn(x, y, BossType[i]));
                    ++counter;
                }
            }
        }
    }

    GameObject Spawn(float x, float y, GameObject type)
    {
        Vector3 offset = new Vector3(x * SizeConstant + Random.Range(-1.5f, 1.5f), 0.2f, y * SizeConstant + Random.Range(-1.5f, 1.5f));

        return (GameObject)Instantiate(type, offset, Quaternion.identity);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public List<Vector3> GetPathTo(Vector3 start, Vector3 target, bool exploredOnly)
    {
        List<Vector3> path = new List<Vector3>();

        int targetX = Mathf.RoundToInt(target.x / SizeConstant);
        int targetY = Mathf.RoundToInt(target.z / SizeConstant);
        int startX = Mathf.RoundToInt(start.x / SizeConstant);
        int startY = Mathf.RoundToInt(start.z / SizeConstant);

        List<Tile> openList = new List<Tile>();
        openList.Add(Tiles[startX][startY]);
        Vector3[][] nodes = new Vector3[SizeX][];

        for (int x = 0; x < SizeX; ++x)
        {
            nodes[x] = new Vector3[SizeY];
            for (int y = 0; y < SizeY; ++y)
            {
                nodes[x][y] = new Vector3(-1, -1, 10000);
            }
        }

        nodes[startX][startY] = new Vector3(startX + 0.5f, startY + 0.5f, 0.0f);


        bool exitReached = false;

        int counter = 0;

        while (openList.Count > 0 && !exitReached)
        {
            int index = 0;

            for (int i = 1; i < openList.Count; ++i)
            {
                if ((openList[i].X - targetX) * (openList[i].X - targetX) +
                    (openList[i].Y - targetY) * (openList[i].Y - targetY) + nodes[openList[i].X][openList[i].Y].z <
                    (openList[index].X - targetX) * (openList[index].X - targetX) +
                    (openList[index].Y - targetY) * (openList[index].Y - targetY) + nodes[openList[index].X][openList[index].Y].z)
                {
                    index = i;
                }
            }

            Tile tile = openList[index];
            openList.RemoveAt(index);

            ++counter;

            if (counter > SizeX * SizeY * 50)
            {
                Debug.Log("No path found!");
                return path;
            }

            for (int i = 0; i < 8; i += 2)
            {
                if (tile.X + OffsetX[i] < 0 || tile.X + OffsetX[i] >= SizeX ||
                    tile.Y + OffsetY[i] < 0 || tile.Y + OffsetY[i] >= SizeY ||
                    Tiles[tile.X + OffsetX[i]][tile.Y + OffsetY[i]].Solid ||
                    (!Tiles[tile.X + OffsetX[i]][tile.Y + OffsetY[i]].Discovered && exploredOnly) ||
                    nodes[tile.X + OffsetX[i]][tile.Y + OffsetY[i]].z < nodes[tile.X][tile.Y].z + 1.0f)
                {
                    continue;
                }

                nodes[tile.X + OffsetX[i]][tile.Y + OffsetY[i]] = new Vector3(tile.X + 0.1f, tile.Y + 0.1f, nodes[tile.X][tile.Y].z + 1.0f);

                if (!openList.Contains(Tiles[tile.X + OffsetX[i]][tile.Y + OffsetY[i]]))
                {
                    openList.Add(Tiles[tile.X + OffsetX[i]][tile.Y + OffsetY[i]]);
                }

                if (tile.X + OffsetX[i] == targetX &&
                    tile.Y + OffsetY[i] == targetY)
                {
                    exitReached = true;
                    break;
                }
            }
        }

        if (exitReached)
        {
            int x = targetX;
            int y = targetY;
            path.Add(new Vector3(targetX * SizeConstant, 0.0f, targetY * SizeConstant));

            while (x != startX || y != startY)
            {
                path.Add(new Vector3(x * SizeConstant, 0.0f, y * SizeConstant));

                int newX = Mathf.FloorToInt(nodes[x][y].x);
                int newY = Mathf.FloorToInt(nodes[x][y].y);
                x = newX;
                y = newY;
            }
        }
        else
        {
            Debug.Log("Nope");
        }

        return path;
    }

    public void Explore(List<Creature> members)
    {
        for (int x = 0; x < SizeX; ++x)
        {
            for (int y = 0; y < SizeY; ++y)
            {
                if (!Tiles[x][y].Solid && !Tiles[x][y].Discovered)
                {
                    foreach (Creature obj in members)
                    {
                        Vector3 dir = new Vector3(x * SizeConstant, 0.0f, y * SizeConstant) - obj.gameObject.transform.position;
                        dir.y = 0.0f;
                        float distance = dir.magnitude;
                        if (distance < obj.SightDistance)
                        {
                            dir.Normalize();
                            Ray ray = new Ray(obj.gameObject.transform.position + Vector3.up, dir);
                            RaycastHit hitInfo;

                            if (!Physics.Raycast(ray, out hitInfo, distance, 1) || hitInfo.distance > distance)
                            {
                                Tiles[x][y].Discovered = true;
                            }
                        }
                    }
                }
            }
        }
    }

    private IEnumerator CloseScreenss(bool won)
    {
        yield return new WaitForSeconds(5.0f);

        if (won)
        {
            int CurrentLevel = PlayerPrefs.GetInt("Current Level", 0);
            int UnlockedLevel = PlayerPrefs.GetInt("Unlocked Level", 0);
            if (CurrentLevel >= UnlockedLevel)
            {
                PlayerPrefs.SetInt("Unlocked Level", CurrentLevel + 1);
                PlayerPrefs.Save();
            }
            Victory.SetActive(true);
        }
        else
        {
            Failure.SetActive(true);
        }
        Destroy(this);
    }

    public void Finish(bool won)
    {
        StartCoroutine(CloseScreenss(won));
    }

}
