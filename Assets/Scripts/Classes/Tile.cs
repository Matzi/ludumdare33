﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    class Tile
    {
        public int X = -1;
        public int Y = -1;
        public bool Solid = true;
        public bool Blocked = false;
        public bool Discovered = false;
        public GameObject Floor;
    }
}
