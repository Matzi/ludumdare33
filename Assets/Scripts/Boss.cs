﻿using UnityEngine;
using System.Collections;

public class Boss : MonoBehaviour {

    public Color BossColor;

	// Use this for initialization
	void Start () {
        Renderer renderer = GetComponentInChildren<Renderer>();
        renderer.material.color = BossColor;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
