============================================
== Eternal Rest
============================================

Created as an entry for Ludum Dare 33 on the 72 hour game jam.
http://ludumdare.com/compo/ludum-dare-33/?action=preview&uid=56958

Play as a wraith condemned to be disturbed by mortals. 
Defend your tomb, separate your attackers, leech their lives, and rest for all eternity.
Or fail and die by sword and magic!


Art by Balázs Szelecki
Programming by Mátyás Bula

Contact: matzigon AT gmail DOT com

Music is free to use resource. See sound_license.txt.

=============================================

This work is licensed under CC 3.0
http://creativecommons.org/licenses/by/3.0/